#!/usr/bin/perl -w
###############################################################################
#                                                                             #
#  TakTuk, a middleware for adaptive large scale parallel remote executions   #
#  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        #
#                                                                             #
#  This program is free software; you can redistribute it and/or modify       #
#  it under the terms of the GNU General Public License as published by       #
#  the Free Software Foundation; either version 2 of the License, or          #
#  (at your option) any later version.                                        #
#                                                                             #
#  This program is distributed in the hope that it will be useful,            #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#  GNU General Public License for more details.                               #
#                                                                             #
#  You should have received a copy of the GNU General Public License          #
#  along with this program; if not, write to the Free Software                #
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA #
#                                                                             #
#  Contact: Guillaume.Huard@imag.fr                                           #
#           Laboratoire LIG - ENSIMAG - Antenne de Montbonnot                 #
#           51 avenue Jean Kuntzmann                                          #
#           38330 Montbonnot Saint Martin                                     #
#           FRANCE                                                            #
#                                                                             #
###############################################################################
use strict;

my $rank = TakTuk::get('rank');
my $count = TakTuk::get('count');
my $own_target = TakTuk::get('target');

print "I'm process $rank (target $own_target) among $count\n";

sub receive() {
    my ($from, $message) = TakTuk::recv(timeout=>(($own_target+1)*3));
    if (not defined($message)) {
        print "Trying to recv: ",TakTuk::error_msg($TakTuk::error), "\n";
	return 0;
    } else {
        print "$rank (target $own_target) received $message from $from\n";
	return 1;
    }
}

sub do_ring($) {
    my $target = shift;
    $target = $rank-1 unless $target;
    my $should_send = ($own_target == $rank-1);

    if ($rank > 1) {
	my $success = receive;
	$should_send = $success unless $target == 'all';
        sleep 1;
    }
    
    if ($should_send) {
        my $next = $rank+1;
        $next = 1 if ($next > $count);
	print "$rank sends a message to $next (target $target)\n";
        if (not TakTuk::send(to=>$next,body=>"[Salut numero $rank]",target=>$target)) {
            print "Trying to send to $next: ",TakTuk::error_msg($TakTuk::error), "\n";
	}
    }
    
    if ($rank == 1) {
	receive;
    }
}

do_ring($ARGV[0]);

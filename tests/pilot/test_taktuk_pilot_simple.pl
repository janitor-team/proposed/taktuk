#!/usr/bin/perl -w -I../../Perl-Module/lib
use strict;

our $taktuk;

sub handler() {
print "Got it !\n";
}

$SIG{INT} = \&handler;

use TakTuk::Pilot;

our @line_counter;

sub output_callback(%) {
    my %parameters = @_;
    my $field = $parameters{fields};
    my $rank = $field->{rank};
    my $argument = $parameters{argument};

    $argument->[$rank] = 1 unless defined($argument->[$rank]);
    print "$field->{host}-$rank : $argument->[$rank] > $field->{line}\n";
    $argument->[$rank]++;
}

die "This script requieres as arguments hostnames to contact\n"
    unless scalar(@ARGV);

$taktuk = TakTuk::Pilot->new();
$taktuk->add_callback(callback=>\&output_callback, stream=>'output',
                      argument=>\@line_counter,
                      fields=>['host', 'rank', 'line']);

# Simple session with a complete command sent by the Pilot
my $result = $taktuk->run(command=>"../../taktuk -s -m ".join(" -m ", @ARGV).
                          " broadcast exec [ ls ]");
if ($result) {
    print "Error $result\n";
    print join(' ',%!)."\n";
}

# Session with a complete command and signal interaction
if (my $pid = fork()) {
    sleep 1;
    kill "INT", $pid;
} else {
    $taktuk->create_session;
    $taktuk->send_command("broadcast exec [ ls ]");
    $result = $taktuk->run(command=>"../../taktuk -s -m ".join(" -m ", @ARGV));
    if (($result == TakTuk::Pilot::ESELEC) and ($!{EINTR})) {
        print "TakTuk::Pilot has been interrupted by a signal !\n";
        print "Terminating now ...\n";
        $taktuk->send_termination;
        $result = $taktuk->continue;
    }
    print "Error $result\n" if $result;
}

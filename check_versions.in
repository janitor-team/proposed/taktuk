#!/usr/bin/perl

use strict;

#####################################################################
# Version managment
package Versions;

sub vcompare($$) {
    my $v1=shift;
    my $v2=shift;

    my @v1=split(/\./,$v1);
    my @v2=split(/\./,$v2);
    while(1) {
	my $s1=shift @v1;
	my $s2=shift @v2;
	if (!defined($s1)) {
	    if (!defined($s2)) {
		return 0;
	    } else {
		return -1;
	    }
	} elsif (!defined($s2)) {
	    return 1;
	}
	my $ret=$s1 <=> $s2;
	if ($ret) {
	    return $ret;
	}
	next;
    }
}

sub new {
    my $this = shift;
    my $versions=shift;
    my $class = ref($this) || $this;
    my $self = { 'ranges'=>[] };
    bless $self, $class;
    $self->_init(@_);
    $self->add_ranges($versions);
    return $self;
}

sub _init(%) {
    my $self=shift;
    if (@_) {
        my %extra = @_;
        @$self{keys %extra} = values %extra;
    }
}

sub add_ranges($$) {
    my $self=shift;
    my $versions=shift;

    my @versions=split(/\s*,\s*/, $versions);
    foreach my $v (@versions) {
	$self->add_range($v);
    }
}

sub add_range($$) {
    my $self=shift;
    my $versions=shift;

    my @versions=split(/\s*-\s*/, $versions);
    my $nb=scalar(@versions);
    if ($nb==0) {
	print STDERR "Empty version!\n";
    } elsif ($nb==1) {
	push @{$self->{'ranges'}}, [$versions[0], $versions[0]];
    } elsif ($nb==2) {
	if (vcompare($versions[0], $versions[1])>0) {
	    print STDERR "Reverted versions ($versions)!\n";
	    push @{$self->{'ranges'}}, [$versions[1], $versions[0]];
	} else {
	    push @{$self->{'ranges'}}, [$versions[0], $versions[1]];
	}
    } else {
	    print STDERR "Invalid versions ($versions)!\n";
    }
}

sub has_version($$) {
    my $self=shift;
    my $version=shift;
 
    foreach my $range (@{$self->{'ranges'}}) {
	if (vcompare($range->[0], $version)<=0
	    && vcompare($range->[1], $version)>=0) {
	    #print STDERR "$version in $range->[0] - $range->[1]\n";
	    return 1;
	}
	#print STDERR "$version not in $range->[0] - $range->[1]\n";
    }
    return 0;
}

#####################################################################
# Load data from file
package Load;

use strict;

my $mode="init";

my $modules;
my $module;
my $API;
my $versions;

my $APIs;

sub parse_versions($) {
    my $versions=shift;

    return new Versions($versions);
}

sub add_module($) {
    my $name=shift;
    
    $module={
	'name' => $name,
    };
    $API=undef;
    $versions=undef;
    push @{$modules}, $module;
}

sub add_info_module($$) {
    my $kind=shift;
    my $value=shift;

    $module->{$kind}=$value;
}

sub add_API_provides($) {
    my $name=shift;
    
    $API={
	'name' => $name,
    };
    $versions=undef;
    push @{$module->{"API-provides"}}, $API;

    if (defined($APIs->{$name})) {
	print STDERR "API $name provided in module ", $module->{'name'},
	" is already provided in module ",
	$APIs->{$name}->{'module'}->{'name'}, "\n";
    } else {
	$APIs->{$name}={
	    'module' => $module,
	    'API' => $API,
	};
    }
}

sub add_version($$) {
    my $version=shift;
    my $versions=shift;
    
    $versions={
	'version' => $version,
	'versions' => $versions,
    };
    push @{$API->{"versions"}}, $versions;
}

sub add_API_depends($) {
    my $name=shift;
    
    $API={
	'name' => $name,
    };
    $versions=undef;
    push @{$module->{"API-depends"}}, $API;
}

sub add_version_depends($$) {
    my $version=shift;
    my $versions=shift;
    
    $versions={
	'version' => $version,
	'versions' => $versions,
    };
    push @{$API->{"versions"}}, $versions;
}

my $reg_versions="[0-9. -]+";

sub read_file($) {
    my $file=shift;
    open(FILE, '<', $file) or die "Cannot open $file: $!";
    while (<FILE>) {
	chomp;
	s/\t/        /g;
	if ($mode eq "init") {
	    if (/^=+$/) {
		$mode="module";
	    }
	    next;
	} 
	if ($mode eq "depver") {
	    if (/^      ([0-9|]+) for ($reg_versions)$/) {
		add_version_depends($1, parse_versions($2));
	    } elsif (! /^      /) {
		$mode="API depends";
	    } else {
		print STDERR "Strange line $_\n";
	    }
	}
	if ($mode eq "API depends") {
	    if (/^    ([^\s]+):.*$/) {
		add_API_depends($1);
		$mode = "depver";
	    } elsif (! /^    /) {
		$mode="module";
	    }
	}
	if ($mode eq "Versions") {
	    if (/^        ([0-9]+): ($reg_versions)$/) {
		add_version($1, parse_versions($2));
	    } elsif (/^      (Versions:)$/) {
		$mode = "Versions";
	    } elsif (! /^        /) {
		$mode="API provides";
	    } else {
		print STDERR "Strange line $_\n";
	    }
	}
	if ($mode eq "API provides") {
	    if (/^    ([^\s]+):.*$/) {
		add_API_provides($1);
	    } elsif (/^      (Versions:)$/) {
		$mode = "Versions";
	    } elsif (! /^    /) {
		$mode="module";
	    } elsif (! /^      /) {
		print STDERR "Strange line $_\n";
	    }
	}
	if ($mode eq "module") {
	    if (/^([^\s]+)$/) {
		add_module($1);
	    }
	    if (/^  Included in: ($reg_versions)$/) {
		add_info_module("included", parse_versions($1));
	    }
	    if (/^  API-provides:$/) {
		$mode="API provides";
	    }
	    if (/^  API-depends:$/) {
		$mode="API depends";
	    }
	}
    }
    close(FILE);
    return { 'modules' => $modules, 'APIs' => $APIs };
}

#####################################################################
# Check datas
package CheckVersions;

my $err=0;

sub check($$) {
    my $infos=shift;
    my $cur_version=shift;

    foreach my $module (@{$infos->{'modules'}}) {
	#print STDERR "$module->{name}\n";
	if (! exists($module->{'included'})) {
	    print STDERR "Module ", $module->{'name'},
	    ": no info about inclusion in TakTuk\n";
	    $err++;
	} elsif (! $module->{'included'}->has_version($cur_version)) {
	    print STDERR "Module ", $module->{'name'},
	    ": not included in TakTuk ", $cur_version, "\n";
	    $err++;
	}
	foreach my $API (@{$module->{'API-provides'}}) {
	    my $found=0;
	    foreach my $version (@{$API->{'versions'}}) {
		if ($version->{'versions'}->has_version($cur_version)) {
		    $found=1;
		    last;
		}
	    }
	    if ($found==0) {
		print STDERR "Module ", $module->{'name'}, ": API provided ",
		$API->{'name'}, ": no version provided for TakTuk ",
		$cur_version, "\n";
		$err++;
	    }
	}
	foreach my $API (@{$module->{'API-depends'}}) {
	    if (!exists($infos->{'APIs'}->{$API->{'name'}})) {
		print STDERR "Module ", $module->{'name'},
		": depends on unknown (not provided) API ",
		$API->{'name'}, "\n";
		$err++;
	    }
	    my $found=0;
	    foreach my $version (@{$API->{'versions'}}) {
		if ($version->{'versions'}->has_version($cur_version)) {
		    $found=1;
		    last;
		}
	    }
	    if ($found==0) {
		print STDERR "Module ", $module->{'name'}, ": requiring API ",
		$API->{'name'}, ": no version for TakTuk ",
		$cur_version, "\n";
		$err++;
	    }
	}
    }
    return $err;
}

#####################################################################
# Main program
package main;
#use Data::Dumper;

use Getopt::Long;
my $version="@TAKTUK_VERSION@";
my $file="@top_srcdir@/API-versions.txt";
my $result = GetOptions ("version=s" => \$version,
			 "file=s"   => \$file);

if (not defined($version)) {
    print STDERR "--version VERSION_TO_CHECK must be given\n";
    exit 1;
}
if (not defined($file)) {
    print STDERR "--file FILE_TO_CHECK must be given\n";
    exit 1;
}

print STDERR "Checking component versions for TakTuk $version in $file\n";

my $infos=Load::read_file($file);
#print Dumper($modules);

my $err=CheckVersions::check($infos, $version);

if ($err > 0) {
    print STDERR "$err error(s) found. Please, fix file $file\n";
    exit 1;
}
exit 0;

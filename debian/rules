#!/usr/bin/make -f

# Disable checks by default: a working non-interactive ssh is required...
NO_CHECKS=1

%:
	dh $@ $(DH_OPTIONS)

override_dh_auto_configure:
	./bootstrap
	dh_auto_configure -- --disable-static

override_dh_auto_test:
ifneq (1,$(NO_CHECKS))
	dh_auto_test
endif

override_dh_missing:
	# We do not install libtool la files
	# We do not reinstall manpage as htmlfile
	# We do not install NEWS, README, ... with dh_install
	dh_missing --fail-missing -X.la \
		-Xusr/share/doc/taktuk/html/taktuk.html \
		-Xusr/share/doc/taktuk/html/taktuk_module.html \
		-Xusr/share/doc/taktuk/html/taktukcomm.html \
		-Xusr/share/doc/taktuk/html/taktuk_pilot.html \
		-Xusr/share/doc/taktuk/NEWS \
		-Xusr/share/doc/taktuk/README \
		-Xusr/share/doc/taktuk/ChangeLog \
		-Xusr/share/doc/taktuk/COPYING \
		-Xusr/share/doc/taktuk/AUTHORS

override_dh_shlibdeps:
	dh_shlibdeps -a -- --warnings=7

override_dh_auto_clean:
	dh_auto_clean
	# Remove files that should not be in the upstream package
	$(RM) taktuk.1 taktuk.html taktuk_module.html taktuk_pilot.html \
		taktukcomm.3 taktukcomm.html

clean:
	dh $@ $(DH_OPTIONS)
	# if this is a git repository, restore removed files that would have been ignored by dpkg-source
	-test -d .git && git checkout -- $$(git status | \
		sed -e '/^#[[:space:]]*deleted:[[:space:]]*/s/^#[[:space:]]*deleted:[[:space:]]*//p;d' | \
		grep -v '^debian/')
